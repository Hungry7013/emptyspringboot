package defaultweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MyController {

    @RequestMapping(value = "/")
    public ModelAndView root(HttpServletRequest request) {

        ModelAndView view = new ModelAndView();
        view.setViewName("hello");
        return view;
    }

}